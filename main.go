package main

import (
	"fmt"
	"gitlab/tyrelsouza/gfsmos/noaa_client"
	"log"
	"os"
	"strings"

	cli "github.com/urfave/cli/v2"
)

func main() {

	app := &cli.App{
		Name:  "GFS MOS",
		Usage: "Parse NOAA's GFS MOS for a given airport",
		Action: func(c *cli.Context) error {
			airport := strings.ToUpper(c.Args().First())
			if airport == "" {
				return cli.NewExitError("please pass the airport's ICAO as an argument", 1)
			}
			api := noaa_client.New(airport)
			pre := api.GetDataRaw()
			fmt.Print(pre)
			return nil
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
