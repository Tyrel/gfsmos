package noaa_client

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
)

type API struct {
	Airport string
}

func New(airport string) *API {
	return &API{airport}
}

func (api *API) GetBody(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	// handling error and doing stuff with body that needs to be unit tested
	return body, err
}

func (api *API) GetDataRaw() string {
	url := fmt.Sprintf("https://www.nws.noaa.gov/cgi-bin/mos/getmav.pl?sta=%s", api.Airport)
	body, _ := api.GetBody(url)

	re := regexp.MustCompile(`(?s)<PRE.*?>(.*)</PRE>`)

	for _, element := range re.FindAllStringSubmatch(string(body), -1) {
		return strings.TrimSpace(element[1])
	}

	return ""
}